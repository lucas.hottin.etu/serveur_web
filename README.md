# SERVEUR DE LUCAS ET RAYD

Serveur web développé en C par Lucas HOTTIN et Rayd TARAFI.

## Utilisation

Une fois que le projet est cloné, il vous faut :
- compiler le serveur à l'aide de la commande make dans le dossier /webserver
- lancer le serveur à l'aide de l'exécutable ./pawnee
- ajouter comme argument la racine de votre site web sans mettre de '/' à la fin
- une fois le serveur lancé vous pouvez accéder à votre site web* :	http://localhost:8080
- pour voir les statistiques de votre serveur allez ici : 		http://localhost:8080/stats

*à condition d'avoir un index.html à la racine de son site web.

Le serveur ne peut répondre que à des requetes GET.

Pour plus de renseignements n'hésitez pas à nous contacter 
- lucas.hottin.etu@univ-lille.fr
- rayd.tarafi.etu@univ-lille.fr
