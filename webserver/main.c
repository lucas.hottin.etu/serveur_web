#include <stdio.h>
#include <string.h>
#include "socket.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <signal.h>
#include "http_parse.h"
#include "stats.h"
#include <semaphore.h>

web_stats *stats;

void traitement_signal(int sig)
{
	printf("Signal %d recu\n", sig);
	waitpid(-1, NULL, WNOHANG);
	printf("Deconnexion d'un client\n");
}

void initialiser_signaux(void)
{
	if (signal(SIGPIPE,SIG_IGN)==SIG_ERR) {
		perror("signal");
		exit(1);
	}
	struct sigaction sa;
	sa.sa_handler = &traitement_signal;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = SA_RESTART;
	if(sigaction(SIGCHLD, &sa, NULL) == -1) {
		perror("sigaction(SIGCHLD)");
		exit(1);
	}
}

char *fgets_or_exit(char *buffer, int size, FILE *stream) 
{
	if(fgets(buffer, size, stream) == NULL) {
		exit(0);
	}
	return buffer;
}

void skip_headers(FILE *client)
{
	char buffer[128] = "";
	while(strcmp(buffer, "\r\n") != 0 && strcmp(buffer, "\n") != 0) { 
		if(fgets(buffer, 128, client) == NULL) {
			exit(0);
		}
	}
}

void send_status(FILE *client, int code, const char *reason_phrase)
{
	fprintf(client, "HTTP/1.1 %d %s\r\n", code, reason_phrase);
}

void send_response(FILE *client, int code, const char *reason_phrase, int lenght, const char *message_body)
{
	send_status(client, code, reason_phrase);
	fprintf(client, "Content-Length: %d\r\n\r\n%s", lenght, message_body);
}

char *rewrite_target(char *target)
{
	if(strlen(target) < 2) {
		return "/index.html";
	}
	if(strchr(target, '?') != NULL) {
		char * chaine = malloc(sizeof(target)+1);
		int i = 0;
		while(target[i] != '?' && target[i] != '\0') {
			chaine[i] = target[i];
			i++;
		}
		i++;
		chaine[i] = '\0';
		return chaine;
	}
	return target;
}

FILE *check_and_open(const char *target, const char *document_root)
{
	char * link = malloc(sizeof(target)+sizeof(document_root)+128);
	strcpy(link, document_root);
	strcat(link, target);
	struct stat fichier;
	if(stat(link, &fichier) == -1) {
		perror("stat");
		return NULL;
	}
	if(S_ISREG(fichier.st_mode)) {
		FILE *fp;
		fp = fopen(link, "r+");
		free(link);
		if(fp == NULL) {
			perror("impossible d'ouvrir target");
			return NULL;
		}
		return fp;
	}
	return NULL;
}

int get_file_size(int fd)
{
	struct stat fichier;
	if(fstat(fd, &fichier) < 0) {
		perror("stat size");
		return -1;
	}
	return fichier.st_size;
}

int copy(FILE *in, FILE *out)
{
	char c;
	while(fread(&c, 1, 1, in) == 1) {
		fwrite(&c, 1, 1, out);
	}
	return 0;
}

void send_stats(FILE *client)
{
	char * message = "{\n\t\"served_connections\":,\n\t\"served_requests\":,\n\t\"ok_200\":,\n\t\"ko_400\":,\n\t\"ko_403\":,\n\t\"ko_404\":,\n\t\"ko_404\":,\n\t\"ko_405\": \"%d\"\n};";
	send_status(client, 200, "OK");
	fprintf(client, "Content-Length: %ld\r\nContent-Type: application/json\r\n\r\n", strlen(message));
	fprintf(client, "{\n\t\"served_connections\":%d,\n\t\"served_requests\":%d,\n\t\"ok_200\":%d,\n\t\"ko_400\":%d,\n\t\"ko_403\":%d,\n\t\"ko_404\":%d,\n\t\"ko_405\":%d\n};", stats->served_connections, stats->served_requests, stats->ok_200, stats->ko_400, stats->ko_403, stats->ko_404, stats->ko_405);

}

int main(int argc, char **argv)
{
	init_stats();
	stats = get_stats();
	//sem_t semaphore;
	//sem_init(&semaphore, 1, 1);
	initialiser_signaux();
	printf("\033[32;01mServeur démarré sur le port 8080\033[00m\n");
	if(argc > 2) {
		perror("arguments");
		exit(1);
	}
	DIR *rep;
	rep = opendir(argv[1]);
	if(rep == NULL) {
		perror("mauvais repertoire");
		exit(0);
	}
	if(chdir(argv[1]) < 0) {
		perror("chdir");
		exit(1);
	}
	int socket_serveur = creer_serveur(8080);
	while(1) {
		int socket_client = accept(socket_serveur, NULL, NULL);
		printf("Nouvelle connection\n");
		stats->served_connections++;
		FILE * fd = fdopen(socket_client, "w+");
		if(socket_client == -1) {
			perror("accept");
			exit(1);
		}
		pid_t pid = fork();
		if(pid == -1) {
			perror("fork");
			exit(1);
		}
		if(pid == 0) {
			// fils
			// boucle qui s'occupe de lire et renvoyer ce que le client envoi
			while(1) {
				char buffer[128] = "";
				fgets_or_exit(buffer, 128, fd);
				stats->served_requests++;
				printf("%s", buffer);
				fflush(fd);
				http_request request;
				FILE * file;
				if(parse_http_request(buffer, &request) == 0) {
					//sem_wait(&semaphore);
					stats->ko_400++;
					//sem_post(&semaphore);
					char * message = "Bad Request\r\n";
				       	send_response(fd, 400, "Bad Request", strlen(message), message);
					fclose(fd);
					return -1;
				}else if(request.method == HTTP_UNSUPPORTED) {
					//sem_wait(&semaphore);
					stats->ko_405++;
					//sem_post(&semaphore);
					char * message = "Method Not Allowed\r\n";
			 		send_response(fd, 405, "Method Not Allowed", strlen(message), message);
					 fclose(fd);
					 return -1;
				}else if(strstr(rewrite_target(request.target), "../") != NULL) {
					//sem_wait(&semaphore);
					stats->ko_403++;
					//sem_post(&semaphore);
					char * message = "Forbiden\r\n";
					send_response(fd, 403, "Forbiden", strlen(message), message);
					fclose(fd);
					return -1;
				}else if(strcmp(rewrite_target(request.target), "/stats") == 0) {
					//sem_wait(&semaphore);
					stats->ok_200++;
					//sem_post(&semaphore);
					skip_headers(fd);
					send_stats(fd);
				}else if((file = check_and_open(rewrite_target(request.target), argv[1])) == NULL) {
					//sem_wait(&semaphore);
					stats->ko_404++;
					//sem_post(&semaphore);
					char * message = "Not Found\r\n";
					send_response(fd, 404, "Not Found", strlen(message), message);
					fclose(fd);
					return -1;
				}else{
					//sem_wait(&semaphore);
					stats->ok_200++;
					//sem_post(&semaphore);
					skip_headers(fd);
					int desc_file = fileno(file);
					send_response(fd, 200, "OK", get_file_size(desc_file), "");
					copy(file, fd);
					fclose(file);
				}
			}
		}
		// pere
		fclose(fd);
	}
	return 0;
}

